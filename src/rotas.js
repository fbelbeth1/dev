import { BrowserRouter, Route, Routes } from "react-router-dom";

import Login from './paginas/Login';
import Home from "./paginas/Home";
import Register from "./paginas/Register";

const Rotas = () => {
    return (
        <BrowserRouter>
            <Routes>
                <Route exact={true} path="/login" element={<Login/>} />
                <Route exact={true} path="/" element={<Home/>} />
                <Route exact={true} path="/register" element={<Register/>} />
            </Routes>
        </BrowserRouter>
    )
}

export default Rotas;