import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

let firebaseConfig = {
    apiKey: "AIzaSyAj3zTyY2qz3kruxSQDP8NmTunWAzzTARU",
    authDomain: "projetopuc-782e0.firebaseapp.com",
    projectId: "projetopuc-782e0",
    storageBucket: "projetopuc-782e0.appspot.com",
    messagingSenderId: "76385950827",
    appId: "1:76385950827:web:31828356d41e78a5fb252e"
  };

  if(!firebase.getApps){
    firebase.initializeApp(firebaseConfig);
  }

  export default firebase;