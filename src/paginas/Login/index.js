import React from 'react';
import { Component } from 'react';
import firebase from '../../Firebase';
import { Link } from 'react-router-dom';
import '../../App.css';
import toastr from 'toastr';
import 'toastr/build/toastr.min.css'


class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      titulo: "Login",
      formulario: [
        { senha: "" },
        { email: "" }
      ],
      resposta: "",
    }

    this.mudarEmail = this.mudarEmail.bind(this);
    this.mudarSenha = this.mudarSenha.bind(this);
    this.login = this.login.bind(this);
  }

  mudarEmail(event) {
    let state = this.state;
    state.formulario.email = event.target.value;
    this.setState(state);
    console.log(state.formulario.email);
  }

  mudarSenha(event) {
    let state = this.state;
    state.formulario.senha = event.target.value;
    this.setState(state);
    console.log(state.formulario.senha);
  }

  async login() {
    await firebase.auth().signInWithEmailAndPassword(this.state.formulario.email, this.state.formulario.senha)
    .then(() => {
      window.location = "/";
    })
    .catch((error) => {
      console.log(error);
      toastr.error("Erro ao realizar login!");
    });
    //let state = this.state;
    //firebase.firestore().collection("usuario").get().then((retorno) => {
    // retorno.forEach(item => {
    //    console.log(item.data().nome);
    //  })
    //});

    //this.setState(state);
    //console.log(state.resposta);
  }

  render() {
    return (
      <section className="vh-100 gradient-custom">
        <div className="container py-5 h-100">
          <div className="row d-flex justify-content-center align-items-center h-100">
            <div className="col-12 col-md-8 col-lg-6 col-xl-5">
              <div className="card bg-dark text-white log">
                <div className="card-body p-5 text-center">

                  <div className="mb-md-5 mt-md-4">

                    <h2 className="fw-bold mb-2 text-uppercase">Login</h2>
                    <p className="text-white-50 mb-5">Por favor informe seu e-mail e senha!</p>

                    <div className="form-outline form-white mb-4">
                      <input id="typeEmailX" className="form-control form-control-lg" name="formulario.email" onChange={(e) => this.mudarEmail(e)} type='text'></input>
                      <label className="form-label" htmlFor="typeEmailX">Email</label>
                    </div>

                    <div className="form-outline form-white mb-4">
                      <input id="typePasswordX" className="form-control form-control-lg" name="formulario.senha" onChange={(e) => this.mudarSenha(e)} type='password'></input>
                      <label className="form-label" htmlFor="typePasswordX">Senha</label>
                    </div>


                    <button className="btn btn-outline-light btn-lg px-5" onClick={this.login}>Acessar</button>

                  </div>

                  <div>
                    <p className="mb-0">Não possui uma conta? <Link to="/register" className="text-white-50 fw-bold">Sign Up</Link>
                    </p>
                  </div>

                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    )
  };
}

export default App;
