import React from 'react';
import { Component } from 'react';
import firebase from '../../Firebase';
import '../../App.css';
import toastr from 'toastr';
import 'toastr/build/toastr.min.css'
import { Link } from 'react-router-dom';


class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      titulo: "Registrar-se",
      formulario: [
        { senha: "" },
        { email: "" },
        { nome: "" },
        { sobrenome: "" },
        { nascimento: "" },
      ],
      resposta: "",
    }

    this.mudarEmail = this.mudarEmail.bind(this);
    this.mudarSenha = this.mudarSenha.bind(this);
    this.mudarNome = this.mudarNome.bind(this);
    this.mudarSobrenome = this.mudarSobrenome.bind(this);
    this.mudarNascimento = this.mudarNascimento.bind(this);
    this.gravar = this.gravar.bind(this);
  }

  mudarEmail(event) {
    let state = this.state;
    state.formulario.email = event.target.value;
    this.setState(state);
    console.log(state.formulario.email);
  }

  mudarSenha(event) {
    let state = this.state;
    state.formulario.senha = event.target.value;
    this.setState(state);
    console.log(state.formulario.senha);
  }

  mudarNome(event) {
    let state = this.state;
    state.formulario.nome = event.target.value;
    this.setState(state);
  }

  mudarSobrenome(event) {
    let state = this.state;
    state.formulario.sobrenome = event.target.value;
    this.setState(state);
  }

  mudarNascimento(event) {
    let state = this.state;
    state.formulario.nascimento = event.target.value;
    this.setState(state);
  }

  gravar() {
    console.log(this.state.formulario.email);
    if(this.state.formulario.email === "" || this.state.formulario.senha === "" || this.state.formulario.nome === "" || this.state.formulario.sobrenome === "" || this.state.formulario.nascimento === "" ||
    this.state.formulario.email === undefined || this.state.formulario.senha === undefined || this.state.formulario.nome === undefined || this.state.formulario.sobrenome === undefined || this.state.formulario.nascimento === undefined
    ){
      toastr.error("Nenhum campo pode estar vazio");
      return;
    }
    firebase.auth().createUserWithEmailAndPassword(this.state.formulario.email, this.state.formulario.senha)
    .then((retorno) => {
      firebase.firestore().collection("usuario").doc(retorno.user.uid).set({
        nome: this.state.formulario.nome,
        sobrenome: this.state.formulario.sobrenome,
        nascimento: this.state.formulario.nascimento
      });
      toastr.success("Cadastro realizado com sucesso!");
    })
    .catch((error) => {
      toastr.error("Erro ao se registrar!\n" + error);
    });
    //let state = this.state;
    //toastr.error("Usuário ou senha incorretos.")
    //this.setState(state);
    //console.log(state.resposta);
  }

  render() {
    return (
      <section className="vh-100 gradient-custom">
        <div className="container py-5 h-100">
          <div className="row d-flex justify-content-center align-items-center h-100">
            <div className="col-12 col-md-8 col-lg-6 col-xl-5">
              <div className="card bg-dark text-white log">
                <div className="card-body p-5 text-center">

                  <div className="mt-md-4">

                    <h2 className="fw-bold mb-2 text-uppercase">Registrar-se</h2>
                    <p className="text-white-50 mb-5">Preencha todos os campos para se registrar!</p>

                    <div className="form-outline form-white mb-4">
                      <input id="typeEmailX" className="form-control form-control-lg" name="formulario.email" onChange={(e) => this.mudarEmail(e)} type='text'></input>
                      <label className="form-label" htmlFor="typeEmailX">Email</label>
                    </div>

                    <div className="form-outline form-white mb-4">
                      <input id="typePasswordX" className="form-control form-control-lg" name="formulario.senha" onChange={(e) => this.mudarSenha(e)} type='password'></input>
                      <label className="form-label" htmlFor="typePasswordX">Senha</label>
                    </div>

                    
                    <div className="form-outline form-white mb-4">
                      <input id="typeNomeX" className="form-control form-control-lg" name="formulario.nome" onChange={(e) => this.mudarNome(e)} type='text'></input>
                      <label className="form-label" htmlFor="typeNomeX">Nome</label>
                    </div>

                    <div className="form-outline form-white mb-4">
                      <input id="typeSobrenomeX" className="form-control form-control-lg" name="formulario.sobrenome" onChange={(e) => this.mudarSobrenome(e)} type='text'></input>
                      <label className="form-label" htmlFor="typeSobrenomeX">Sobrenome</label>
                    </div>
                    
                    
                    <div className="form-outline form-white mb-4">
                      <input id="typeNascimentoX" className="form-control form-control-lg" name="formulario.nascimento" onChange={(e) => this.mudarNascimento(e)} type='date'></input>
                      <label className="form-label" htmlFor="typeNascimentoX">Data Nascimento</label>
                    </div>

                    <button className="btn btn-outline-light btn-lg px-5" onClick={this.gravar}>Registrar-se</button>

                    <div>
                      <p className="mt-3">Já possui uma conta? <Link to="/login" className="text-white-50 fw-bold">Login In</Link>
                    </p>
                  </div>
                  </div>

                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    )
  };
}

export default App;
