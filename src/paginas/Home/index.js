import React from 'react';
import { Component } from 'react';
import firebase from '../../Firebase';
import toastr from 'toastr';


class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      nome: "",
      sobrenome: "",
      nascimento: ""
    }
  }

  async componentDidMount() {
    await firebase.auth().onAuthStateChanged(async (usuario) => {
      if (usuario) {
        var uid = usuario.uid;

        await firebase.firestore().collection("usuario").doc(uid).get()
          .then((retorno) => {
            this.setState({
              nome: retorno.data().nome,
              sobrenome: retorno.data().sobrenome,
              nascimento: retorno.data().nascimento
            })
          });
      }
      else{
        window.location.href = "/login";
      }
    })
  }

  async logout(){
    await firebase.auth().signOut()
    .then(() => {
      window.location = "/login";
    })
    .catch((error) => {
      console.log(error);
      toastr.error("Erro ao realizar logout!");
    });
  }

  render() {
    return (
      <div>
        <div>
        <button className="btn btn-outline-light btn-lg px-5" onClick={this.logout}>Logout</button>
        </div>

        <div className="container d-flex justify-content-center">
          <ul className="list-group mt-5 text-white">
            <li className="list-group-item d-flex justify-content-between align-content-center">

              <div className="d-flex flex-row">
                <img src="https://img.icons8.com/?size=256&id=17085&format=png" alt="info" width="40" />
                <div className='d-flex flex-column justify-content-center align-items-center'>
                  <h6> Nome: {this.state.nome}</h6>
                </div>
              </div>

            </li>

            <li className="list-group-item d-flex justify-content-between align-content-center">

              <div className="d-flex flex-row">
                <img src="https://img.icons8.com/?size=256&id=17085&format=png" alt="info" width="40" />
                <div className='d-flex flex-column justify-content-center align-items-center'>
                  <h6>Sobrenome: {this.state.sobrenome}</h6>
                </div>
              </div>

            </li>

            <li className="list-group-item d-flex justify-content-between align-content-center">

              <div className="d-flex flex-row">
                <img className="pr-5" src="https://img.icons8.com/?size=256&id=17085&format=png" alt="info" width="40" />
                <div className='d-flex flex-column justify-content-center align-items-center'>
                  <h6>Nascimento: {this.state.nascimento}</h6>
                </div>
              </div>
            </li>




          </ul>

        </div>
      </div>
    )
  };
}

export default Home;
